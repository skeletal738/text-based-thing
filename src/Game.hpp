#pragma once

#include <memory>
class Console; // Forward declaration

class Game {
private:
	bool Running;
	std::shared_ptr<Console> mainConsole;
public:
	Game();
	void Begin();
	void Exit();

};