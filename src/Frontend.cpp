#include "Frontend.hpp"
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

Command::Command(const std::string& text, void(*cmdCallback)(Game&, const std::vector<std::string>& args)) {
	this->cmdText = text;
	this->callback = cmdCallback;
}

void Command::Entered(Game& caller, const std::vector<std::string>& args) {
	callback(caller, args);
}

Console::Console() {
	return;
}

bool Console::BindCommand(const std::string& cmdText) {
	Commands.push_back(std::make_shared<Command>("test", commandFuncs::testFunc));
	Commands.push_back(std::make_shared<Command>("exit", commandFuncs::exit));
	Commands.push_back(std::make_shared<Command>("walk", commandFuncs::walk));
	return true;
}

void Console::TakeInput(Game& game) {
	bool valid = false;
	std::string input;
	std::cout << "> ";
	std::getline(std::cin, input);
	// std::cout << "You typed: " << input << std::endl;
	std::transform(input.begin(), input.end(), input.begin(), ::tolower);

	std::istringstream iss(input);
	std::vector<std::string> split {std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>{} };

	for(std::shared_ptr<Command> c : Commands){
		if (std::string(split[0]) == c->cmdText) {
			std::vector<std::string> args;
			for (std::size_t i = 1; i < split.size(); i++) {
				args.push_back(split[i]);
			}
			valid = true;
			c->Entered(game, args);
		}
	}
	if (!valid) {
		std::cout << "\"" << split[0] << "\"" << " is not a valid action.\n";
	}
}