#include "Frontend.hpp"
#include <algorithm>
#include <string.h> 


void commandFuncs::testFunc(Game& game, const std::vector<std::string>& args) {
	std::cout << "TestFunc was called.\n";
	std::cout << "Oh and by the way, here are the arguments: \n";
	for (const std::string& s : args) {
		std::cout << s;
		std::cout << " ";
	}
	std::cout << "\n";
}
void commandFuncs::exit(Game& game, const std::vector<std::string>& args) {
	std::string response;
	std::cout << "Are you sure you want to exit\nY/N > ";
	std::getline(std::cin, response);
	std::transform(response.begin(), response.end(), response.begin(), ::tolower);
	if (response == std::string("y")){
		game.Exit();
	}
}

void commandFuncs::walk(Game & game, const std::vector<std::string>& args) {
	if (args.empty()) {
		std::cout << "Which direction do you want to walk in? (North/East/South/West)\n";
	}
}
