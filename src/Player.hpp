#pragma once
#include "common.hpp"

class Player {
public:
	Player(const int& x, const int& y);
	int x;
	int y;
};