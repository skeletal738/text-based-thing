#include "Frontend.hpp"
#include "Game.hpp"

// TODO: Vector based world, where each room is on a 2d plane, to aid navigation and data storage
// Also, being able to "look" into other rooms


Game::Game() {
	mainConsole = std::make_shared<Console>();
}

void Game::Begin() {
	Running = true;
	mainConsole->BindCommand("garbage");
	std::cout << "You see naught but black.\n";
	while(Running) {
		mainConsole->TakeInput(*this);
	}
}

void Game::Exit() {
	Running = false;
}