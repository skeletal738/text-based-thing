#pragma once
#include <vector>
#include <map>
#include <memory>
#include "common.hpp"
class Room {
private:
	int x;
	int y;
	std::string lookText;
	std::string distantLookText;
public:
	Room(int x, int y);
	void Look(bool distant);
};

class World {
	std::vector<std::vector<Room>> Rooms;
public:
	World();
	void LoadWorld(const std::string& worldFile);
};