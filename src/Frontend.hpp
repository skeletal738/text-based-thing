#pragma once
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include "Game.hpp"

class commandFuncs {  // Define built-in command functions here.
public:
	static void testFunc(Game& game, const std::vector<std::string>& args);
	static void exit(Game& game, const std::vector<std::string>& args);
	static void walk(Game& game, const std::vector<std::string>& args);
};

class Command{
public:
	Command(const std::string& text, void(*cmdCallback)(Game&, const std::vector<std::string>& args));
	std::string cmdText;
	void Entered(Game& caller, const std::vector<std::string>& args);
private:
	void (*callback) (Game&, const std::vector<std::string>& args);
};

class Console{
public:
	Console();
	std::vector < std::shared_ptr < Command > > Commands;
	bool BindCommand(const std::string& cmdText);
	void TakeInput(Game& game);
	bool Running;
};